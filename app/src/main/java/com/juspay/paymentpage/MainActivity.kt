package com.juspay.paymentpage

import `in`.juspay.services.HyperServices
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.webkit.WebView

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        HyperServicesHolder.initialize(this)
        HyperServicesHolder.startPrefetch()
        WebView.setWebContentsDebuggingEnabled(true);
    }

    public fun startInitiateActivity(view: View) {
        val initiateActivityIntent = Intent(this, InitiateActivity::class.java)
        startActivity(initiateActivityIntent)
    }
}