package com.juspay.paymentpage

import `in`.juspay.hypersdk.core.MerchantViewType
import `in`.juspay.hypersdk.data.JuspayResponseHandler
import `in`.juspay.hypersdk.ui.HyperPaymentsCallback
import `in`.juspay.hypersdk.ui.JuspayWebView
import android.opengl.Visibility
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import android.webkit.WebViewClient
import android.widget.Button
import android.widget.ProgressBar
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.widget.ContentLoadingProgressBar
import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import org.json.JSONObject


class ProcessActivity : AppCompatActivity() {

    private lateinit var volleyQueue: RequestQueue
    private var isSignatureFetched = false
    private lateinit var signature:String
    private lateinit var orderDetails:JSONObject
    private lateinit var orderID:String

    private lateinit var payButton:Button
    private lateinit var loader:ProgressBar



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_process)
        volleyQueue = Volley.newRequestQueue(this)

        payButton = findViewById(R.id.pay_button)
        loader = findViewById(R.id.loader)

        if(PayloadConstants.v2Integ)
            fetchSignature()
        else {
            isSignatureFetched = true
            processV1()
        }
    }

    private fun fetchSignature() {
        orderID = Payload.generateOrderID()
        orderDetails = Payload.getOrderDetails(orderID)
        val signatureURL = PayloadConstants.signatureURL + "?payload=" + orderDetails.toString()

        val stringRequest =
            StringRequest(
                Request.Method.GET, signatureURL,
                Response.Listener<String> { signatureResponse ->
                    signature = signatureResponse
                    isSignatureFetched = true
                },
                Response.ErrorListener {
                    Toast.makeText(this, "Sign API Failed", Toast.LENGTH_SHORT).show()
                    Log.e("Failure in signature: ", it.toString())
                }
            )
        volleyQueue.add(stringRequest)
    }

    fun processV1(){
        supportActionBar?.hide()
        showLoader()
        HyperServicesHolder.processSDK(
            this,
            Payload.getSDKPayload(Payload.getProcessV1Payload()),
            generateHyperPaymentsCallback()
        )
    }


    fun startPayments(view: View){
        if(!PayloadConstants.v2Integ){
            processV1()
            return;
        }
        if(isSignatureFetched){
            supportActionBar?.hide()
            showLoader()
            HyperServicesHolder.processSDK(
                this,
                Payload.getSDKPayload(
                    Payload.getProcessPayload( orderID, orderDetails, signature )
                ),
                generateHyperPaymentsCallback()
            )
        } else {
            Toast.makeText(this, "Fetching Signature... Please wait", Toast.LENGTH_SHORT).show()
        }
    }

    private fun generateHyperPaymentsCallback(): HyperPaymentsCallback {
        return object : HyperPaymentsCallback {
            override fun onStartWaitingDialogCreated( view: View?) {}

            override fun createJuspaySafeWebViewClient(): WebViewClient? { return null;}

            override fun onWebViewReady(juspayWebView: JuspayWebView) {}

            override fun onEvent(
                jsonObject: JSONObject,
                juspayResponseHandler: JuspayResponseHandler
            ) {
                Log.d("Inside OnEvent ", "initiate")
                try {
                    val event = jsonObject.getString("event")
                    when (event) {
                        "initiate_result" -> Log.wtf("initiate_result", jsonObject.toString())
                        "process_result" -> {
                            supportActionBar?.show()
                            Log.wtf("process_result", jsonObject.toString())
                        }
                        "hide_loader" -> hideLoader()
                        else -> {}
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun getMerchantView(viewGroup: ViewGroup, mViewType : MerchantViewType): View? {
                return null
            }
        }
    }

    override fun onBackPressed() {
        val handleBackPress = HyperServicesHolder.handleBackPress()
        if (handleBackPress) {
            super.onBackPressed()
        }
    }

    private fun showLoader() {
        payButton.visibility = GONE
        loader.visibility = VISIBLE
    }

    private fun hideLoader() {
        payButton.visibility = VISIBLE
        loader.visibility = GONE
    }
}