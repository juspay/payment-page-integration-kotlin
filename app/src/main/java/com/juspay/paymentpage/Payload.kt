package com.juspay.paymentpage

import org.json.JSONArray
import org.json.JSONObject
import java.util.*
import kotlin.collections.ArrayList
import kotlin.random.Random


object Payload {

    // generator utils

    fun generateOrderID(): String {
        return "R" + Random.nextInt(100000, 999999)
    }

    private fun getTimeStamp(): String {
        return ""+System.currentTimeMillis()
    }

    private fun generateRequestID(): String {
        return ""+ UUID.randomUUID()
    }

    fun getInitiatePayload(signaturePayload: JSONObject, signature: String): JSONObject {
        val initiatePayload = JSONObject()
        initiatePayload.put("action", PayloadConstants.initAction)
        initiatePayload.put("clientId", PayloadConstants.clientId)
        initiatePayload.put("merchantKeyId", PayloadConstants.merchantKeyId)
        initiatePayload.put("signaturePayload", signaturePayload.toString());
        initiatePayload.put("signature", signature);
        initiatePayload.put("environment", PayloadConstants.environment)
        return initiatePayload
    }

    fun getInitPayloadV1(clientAuthToken: String): JSONObject {
        val initPayloadV1 = JSONObject()
        initPayloadV1.put("action", PayloadConstants.initAction)
        initPayloadV1.put("clientId", PayloadConstants.clientId)
        initPayloadV1.put("environment", PayloadConstants.environment)
        initPayloadV1.put("clientAuthToken", clientAuthToken)
        initPayloadV1.put("merchantId", PayloadConstants.merchantId)
        initPayloadV1.put("customerId", PayloadConstants.customerId)
        initPayloadV1.put("emailAddress", PayloadConstants.emailAddress)
        initPayloadV1.put("mobileNumber", PayloadConstants.mobileNumber)
        return initPayloadV1
    }

    fun getInitiateSignaturePayload(): JSONObject {
        val initSignaturePayload = JSONObject()
        initSignaturePayload.put("first_name", PayloadConstants.firstName)
        initSignaturePayload.put("last_name", PayloadConstants.lastName)
        initSignaturePayload.put("mobile_number", PayloadConstants.mobileNumber)
        initSignaturePayload.put("email_address", PayloadConstants.emailAddress)
        initSignaturePayload.put("customer_id", PayloadConstants.customerId)
        initSignaturePayload.put("timestamp", getTimeStamp());
        initSignaturePayload.put("merchant_id", PayloadConstants.merchantId)
        return initSignaturePayload
    }

    fun getProcessPayload(orderID: String, orderDetails: JSONObject, signature: String): JSONObject {
        val processPayload = JSONObject()
        processPayload.put("action", PayloadConstants.processAction)
        processPayload.put("merchantId", PayloadConstants.merchantId)
        processPayload.put("clientId", PayloadConstants.clientId)
        processPayload.put("amount", PayloadConstants.amount)
        processPayload.put("customerId", PayloadConstants.customerId)
        processPayload.put("customerMobile", PayloadConstants.mobileNumber)

        val endUrlArr: ArrayList<String?> = ArrayList(
            Arrays.asList(
                ".*sandbox.juspay.in\\/thankyou.*",
                ".*sandbox.juspay.in\\/end.*",
                ".*localhost.*",
                ".*api.juspay.in\\/end.*"
            )
        )
        val endUrls = JSONArray(endUrlArr)
        processPayload.put("endUrls", endUrls)
        processPayload.put("merchantKeyId",PayloadConstants.merchantKeyId)
        processPayload.put("orderId", orderID)
        processPayload.put("orderDetails", orderDetails.toString())
        processPayload.put("signature", signature)
        processPayload.put("language", PayloadConstants.language)

        return processPayload
    }

    fun getProcessV1Payload(): JSONObject {
        val processPayload = JSONObject()
        processPayload.put("action", PayloadConstants.processAction)
        processPayload.put("merchantId", PayloadConstants.merchantId)
        processPayload.put("clientId", PayloadConstants.clientId)
        processPayload.put("orderId", generateOrderID())
        processPayload.put("amount", PayloadConstants.amount)
        processPayload.put("customerId", PayloadConstants.customerId)
        processPayload.put("customerEmail", PayloadConstants.emailAddress)
        processPayload.put("customerMobile", PayloadConstants.mobileNumber)
        processPayload.put("clientAuthToken", PayloadConstants.clientAuthToken)
        processPayload.put("endUrls", JSONArray())
        processPayload.put("language", "english")
        return processPayload
    }

    fun getOrderDetails(orderID: String): JSONObject {
        val orderDetails = JSONObject()
        orderDetails.put("order_id", orderID);
        orderDetails.put("first_name", PayloadConstants.firstName)
        orderDetails.put("last_name", PayloadConstants.lastName)
        orderDetails.put("mobile_number", PayloadConstants.mobileNumber)
        orderDetails.put("email_address", PayloadConstants.emailAddress)
        orderDetails.put("customer_id", PayloadConstants.customerId)
        orderDetails.put("timestamp", getTimeStamp())
        orderDetails.put("merchant_id", PayloadConstants.merchantId)
        orderDetails.put("amount", PayloadConstants.amount)
        orderDetails.put("return_url", PayloadConstants.returnUrl)
        orderDetails.put("description", "order_description")
        return orderDetails
    }

    fun getSDKPayload(payload: JSONObject): JSONObject {
        val sdkPayload = JSONObject()
        sdkPayload.put("requestId", generateRequestID())
        sdkPayload.put("service", PayloadConstants.service)
        sdkPayload.put("payload", payload);
        sdkPayload.put("betaAssets", PayloadConstants.betaAssets)
        return sdkPayload
    }

    fun getPrefetchPayload(): JSONObject {
        val preFetchPayload = JSONObject()
        val innerPayload = JSONObject()
        val servicesArray = ArrayList(Arrays.asList(PayloadConstants.service))
        val services = JSONArray(servicesArray)
        innerPayload.put("clientId", PayloadConstants.clientId)
        preFetchPayload.put("services", services)
        preFetchPayload.put("betaAssets", PayloadConstants.betaAssets)
        preFetchPayload.put("payload", innerPayload)
        return preFetchPayload
    }

}