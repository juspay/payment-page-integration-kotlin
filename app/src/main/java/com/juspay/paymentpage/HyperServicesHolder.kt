package com.juspay.paymentpage

import `in`.juspay.hypersdk.core.MerchantViewType
import `in`.juspay.hypersdk.data.JuspayResponseHandler
import `in`.juspay.hypersdk.ui.HyperPaymentsCallback
import `in`.juspay.hypersdk.ui.JuspayWebView
import `in`.juspay.services.HyperServices
import android.view.View
import android.view.ViewGroup
import android.webkit.WebViewClient
import androidx.fragment.app.FragmentActivity
import org.json.JSONObject
import java.util.*

object HyperServicesHolder: HyperPaymentsCallback {

    lateinit var fragmentActivity: FragmentActivity
    lateinit var hyperServices: HyperServices
    lateinit var hyperServiceCallback: HyperPaymentsCallback

    enum class EventsType {
        onEvent, onStartWaitingDialogCreated, onWebViewReady, getMerchantView
    }

    private val eventQueue: Queue<QueuedEvents> = LinkedList()

    var isInitiated = false

    fun initialize(activity: FragmentActivity) {
        fragmentActivity = activity
        hyperServices = HyperServices(activity)
    }

    fun startPrefetch(){
        HyperServices.preFetch(fragmentActivity, Payload.getPrefetchPayload())
    }

    fun initiateSDK(activity: FragmentActivity, initiatePayload: JSONObject){
        hyperServices.initiate(activity, initiatePayload, this)
    }

    fun processSDK(activity: FragmentActivity, processPayload: JSONObject, paymentsCallback: HyperPaymentsCallback){
        hyperServiceCallback = paymentsCallback
        runQueueEvents() // executing events in queue when callback is set
        hyperServices.process(activity, processPayload)
    }

    // callback handlers which add events to queue when callback is not set
    // queued events are fired later when call back is set during process call

    override fun onEvent(event: JSONObject?, handler: JuspayResponseHandler?) {
        val eventName = if(event == null) "" else event.optString("event", "")
        if(eventName == "initiate_result")
            isInitiated = true

        if(!this::hyperServiceCallback.isInitialized){
            val qEvent = QueuedEvents()
            qEvent.eventType = EventsType.onEvent
            qEvent.event = event
            qEvent.handler = handler
            eventQueue.add(qEvent)
        } else {
            hyperServiceCallback.onEvent(event, handler)
        }
    }

    override fun onStartWaitingDialogCreated(parent: View?) {
        if(!this::hyperServiceCallback.isInitialized) {
            val qEvent = QueuedEvents()
            qEvent.eventType = EventsType.onStartWaitingDialogCreated
            qEvent.parent = parent
            eventQueue.add(qEvent)
        } else {
            hyperServiceCallback.onStartWaitingDialogCreated(parent);
        }
    }

    override fun onWebViewReady(webView: JuspayWebView?) {
        if(!this::hyperServiceCallback.isInitialized){
            val qEvent = QueuedEvents()
            qEvent.eventType = EventsType.onWebViewReady
            qEvent.jpWebView = webView
            eventQueue.add(qEvent)
        } else {
            hyperServiceCallback.onWebViewReady(webView)
        }
    }

    override fun getMerchantView(parent: ViewGroup?, viewType: MerchantViewType): View? {
        if(!this::hyperServiceCallback.isInitialized){
            val qEvent = QueuedEvents()
            qEvent.eventType = EventsType.getMerchantView
            qEvent.viewGroup = parent
            qEvent.mViewType = viewType
            eventQueue.add(qEvent)
        } else {
            hyperServiceCallback.getMerchantView(parent, viewType)
        }
        return null
    }

    override fun createJuspaySafeWebViewClient(): WebViewClient? {
        return null;
    }

    private fun runQueueEvents() {
        val head = eventQueue.poll()
        if (head != null) {
            when (head.eventType) {
                EventsType.onEvent ->
                    if (this::hyperServiceCallback.isInitialized)
                        hyperServiceCallback.onEvent(head.event, head.handler)

                EventsType.onStartWaitingDialogCreated ->
                    if (this::hyperServiceCallback.isInitialized)
                        hyperServiceCallback.onStartWaitingDialogCreated(head.parent)

                EventsType.onWebViewReady ->
                    if (this::hyperServiceCallback.isInitialized)
                        hyperServiceCallback.onWebViewReady(head.jpWebView)

                EventsType.getMerchantView ->
                    if (this::hyperServiceCallback.isInitialized)
                        hyperServiceCallback.getMerchantView(head.viewGroup, head.mViewType)

                else -> {}
            }

            runQueueEvents()
        }
    }
    
    fun handleBackPress(): Boolean {
        if(this::hyperServices.isInitialized) {
            // returns not of
            // if hyper-services is handling backPress
            return !hyperServices.onBackPressed()
        }
        return true;
    }
}

internal class QueuedEvents {
    var event: JSONObject? = null
    var handler: JuspayResponseHandler? = null
    var eventType: HyperServicesHolder.EventsType? = null
    var viewGroup: ViewGroup? = null
    var parent: View? = null
    var mViewType: MerchantViewType? = null
    var jpWebView: JuspayWebView? = null
}