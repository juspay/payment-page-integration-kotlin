package com.juspay.paymentpage

object PayloadConstants {
    val service = "in.juspay.hyperpay"
    val mobileNumber = "9592329220"
    val clientId = "curefit_android"
    val firstName = "Test"
    val lastName = "User"
    val emailAddress = "test007@gmail.com"
    val customerId = "9592329220"
    val merchantId = "curefit"
    val initAction = "initiate"
    val processAction = "emi"
    val merchantKeyId = "3452"
    val environment = "sandbox"
    val amount = "20000.0"
    val returnUrl = "https://sandbox.juspay.in/end"
    val language = "english"
    val signatureURL = "https://dry-cliffs-89916.herokuapp.com/sign-hyper-beta"
    val betaAssets = true
    val v2Integ = true
    val clientAuthToken = "tkn_1ad13de216164b929cb065ae309eb29a"
}