package com.juspay.paymentpage

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley

class InitiateActivity : AppCompatActivity() {

    private lateinit var volleyQueue:RequestQueue
    private var isInitiateDone = false


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_initiate)

        volleyQueue = Volley.newRequestQueue(this)
        if(PayloadConstants.v2Integ)
            signAndInitiate()
        else
            getSessionAndInitiate()
    }

    private fun getSessionAndInitiate(){
        HyperServicesHolder.initiateSDK(
            this,
            Payload.getSDKPayload(Payload.getInitPayloadV1(PayloadConstants.clientAuthToken))
        )
        isInitiateDone = true
    }

    private fun signAndInitiate(){

        val signaturePayload = Payload.getInitiateSignaturePayload()
        val signatureURL = PayloadConstants.signatureURL + "?payload=" + signaturePayload.toString()

        val stringRequest =
            StringRequest(Request.Method.GET, signatureURL,
                Response.Listener<String> {
                    signature ->
                    HyperServicesHolder.initiateSDK(
                        this,
                        Payload.getSDKPayload(
                            Payload.getInitiatePayload(signaturePayload, signature)
                        )
                    )
                    isInitiateDone = true
                },
                Response.ErrorListener {
                    Toast.makeText(this, "Sign API Failed", Toast.LENGTH_SHORT).show()
                    Log.e("Failure in signature: ", it.toString())
                }
            )
        volleyQueue.add(stringRequest)
    }

    fun startInterMediateActivity(view: View) {
        if(isInitiateDone){
            val intermediateIntent = Intent(this, IntermediateActivity::class.java)
            startActivity(intermediateIntent)
        } else {
            Toast.makeText(this, "Initiate in Progress... Please wait", Toast.LENGTH_LONG).show()
        }
    }
}