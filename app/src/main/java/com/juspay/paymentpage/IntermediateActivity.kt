package com.juspay.paymentpage

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View

class IntermediateActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_intermediate)
    }

    fun startProcessActivity(view: View){
        val processIntent = Intent(this, ProcessActivity::class.java)
        startActivity(processIntent)
    }
}